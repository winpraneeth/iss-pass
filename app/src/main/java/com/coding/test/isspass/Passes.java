package com.coding.test.isspass;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by winpr on 3/12/2018.
 */

class Passes implements Serializable {
    private int duration;
    @SerializedName("risetime")
    private int time;

    int getDuration() {
        return duration;
    }

    void setDuration(int duration) {
        this.duration = duration;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }
}
