package com.coding.test.isspass;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AsyncResponse {

    private static final int MY_ACCESS_FINE_LOCATION = 55;
    private static final int REQUEST_CHECK_SETTINGS = 65;
    private ProgressDialog progressDialog;
    private LocationAsync locationAsync;
    private double latitude;
    private double longitude;
    private PassesAdapter passesAdapter;
    private ArrayList<Passes> passList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView pointer = findViewById(R.id.location);
        RecyclerView passView = findViewById(R.id.pass_list);

        passesAdapter = new PassesAdapter(MainActivity.this, passList);
        passView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        passView.setAdapter(passesAdapter);

        pointer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**
                   *To check if that permission is granted or not. Using checkSelfPermission we check if
                   *the permission is granted and if not we request the user using requestPermissions
                 */
                if (ContextCompat.checkSelfPermission(MainActivity.this,
                        android.Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                            android.Manifest.permission.ACCESS_FINE_LOCATION)) {


                    } else {
                        // No explanation needed, we can request the permission.
                        ActivityCompat.requestPermissions(MainActivity.this,
                                new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                                MY_ACCESS_FINE_LOCATION);
                    }
                } else {
                    getLocation();
                }
            }
        });
    }

    private void getLocation() {
        showDialogue("Checking location settings!");
        if (locationAsync != null)
            locationAsync.cancel(true);
        locationAsync = null;
        if (locationAsync == null) {
            locationAsync = new LocationAsync(this);
            LocationAsync.delegate = this;
        }
        locationAsync.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void dismissDialogue() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }


    public void showDialogue(String message) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
        }
        progressDialog.setMessage(message);
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            // As the user chooses Allow or deny Activity’s onRequestPermissionResult is called and here we handle what happens if the user chooses either allow or deny.
            case MY_ACCESS_FINE_LOCATION: {
                for (int i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        try {
                            getLocation();
                        } catch (SecurityException sx) {
                            // Handling security exception
                        }
                    } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {

                    }
                }
            }
            break;
            default: {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // When returned back to the activity catch the result from Location Settings and proceed accordingly
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            if (resultCode == RESULT_OK) {
                if (locationAsync != null)
                    locationAsync.cancel(true);
                locationAsync = null;
                getLocation();
            }
        }
    }

    @Override
    public void processFinish(Location location) {
        // latitude and longitude of the user's location
        latitude = location.getLatitude();
        longitude = location.getLongitude();

        dismissDialogue();
        passList.clear();
        getISSPasses();
    }

    private void getISSPasses() {
        String url = "http://api.open-notify.org/iss-pass.json?lat=" + latitude + "&lon=" + longitude;
        StringRequest getQuery = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonResponse = new JSONObject(response);

                    JSONArray itemsArray = jsonResponse.getJSONArray("response");
                    for (int i = 0; i < itemsArray.length(); i++) {
                        Passes passes = new Passes();
                        JSONObject jsonObject = itemsArray.getJSONObject(i);

                        int duration = jsonObject.getInt("duration");
                        int time = jsonObject.getInt("risetime");

                        passes.setDuration(duration);
                        passes.setTime(time);

                        passList.add(passes);
                    }

                    passesAdapter.setPassesList(passList);
                    passesAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        int socketTimeout = 15000;//15 seconds
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        getQuery.setRetryPolicy(policy);
        VolleySingleton.getInstance(MainActivity.this).addToRequestQueue(getQuery);

    }

}
