package com.coding.test.isspass;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by winpr on 3/12/2018.
 */

class PassesAdapter extends RecyclerView.Adapter<PassesAdapter.MyViewHolder> {

    private ArrayList<Passes> passList = new ArrayList<>();
    private Context mContext;

    PassesAdapter(Context mContext, ArrayList<Passes> passList) {
        this.passList = passList;
        this.mContext = mContext;
    }

    @Override
    public PassesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pass_list_item, parent, false);
        return new PassesAdapter.MyViewHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Passes passes = passList.get(position);
        String time = "Time: ";
        String duration = "Duration: ";

        Date date = new Date((long)passes.getTime()*1000);
        DateFormat ft = new SimpleDateFormat("yyyy-MM-dd");

        holder.time.setText(time + ft.format(date));
        holder.duration.setText(duration + passes.getDuration());

    }

    void setPassesList(ArrayList<Passes> passList) {
        this.passList = passList;
    }

    @Override
    public int getItemCount() {
        return passList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView duration;
        private TextView time;

        MyViewHolder(View itemView) {
            super(itemView);
            duration = itemView.findViewById(R.id.duration);
            time = itemView.findViewById(R.id.time);


        }
    }
}

