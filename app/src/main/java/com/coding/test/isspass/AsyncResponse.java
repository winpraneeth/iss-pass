package com.coding.test.isspass;

import android.location.Location;

/**
 * Created by winpr on 3/12/2018.
 */

public interface AsyncResponse {

    void processFinish(Location location);
}
