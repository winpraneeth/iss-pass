package com.coding.test.isspass;

import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.util.Locale;

/**
 * Created by winpr on 3/12/2018.
 */

class LocationAsync extends AsyncTask<String, Void, String> implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    static AsyncResponse delegate = null;
    private Boolean servicesAvailable = false;
    private Activity activity;
    private double latitude = 0.;
    private double longitude = 0.0;
    private LocationRequest mLocationRequest;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private static final long UPDATE_INTERVAL = 1000;
    private static final long FASTEST_INTERVAL = 1000;
    private boolean isStopped;
    private static final int REQUEST_CHECK_SETTINGS = 65;

    LocationAsync(Activity activity) {
        this.activity = activity;
        setup();
    }

    private void setup() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(activity);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
            }
        };

        if (servicesConnected()) {

            buildGoogleApiClient();  // used to access the Google APIs provided in the Google Play services
            createLocationRequest(); // Create the location request to start receiving updates.
        }
        mGoogleApiClient.connect();
    }

    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(activity)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        servicesAvailable = servicesConnected();
    }

    private boolean servicesConnected() {
        // Returns the instance of GoogleApiAvailability and Verifies that Google Play services is installed and enabled on this device
        int resultCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(activity);

        // If Google Play services is available
        return ConnectionResult.SUCCESS == resultCode;
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected void onCancelled() {
        //progDailog.dismiss();
        isStopped = true;
    }

    @Override
    protected String doInBackground(String... strings) {
        if (isLocationEnabled(activity)) {
            while (this.latitude == 0.0 && !isStopped) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } else {
            displaySettings();
        }

        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        if (this.latitude != 0.0) {
            Location location = new Location("");
            location.setLatitude(latitude);
            location.setLongitude(longitude);
            delegate.processFinish(location);
        }
    }

    private void displaySettings() {
        //To check if location is turned on/off of the user’s device.
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
        Task<LocationSettingsResponse> result = LocationServices.getSettingsClient(activity).checkLocationSettings(builder.build());
        result.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> result) {
                LocationSettingsResponse status = null;
                try {
                    status = result.getResult(ApiException.class);

                } catch (ApiException exception) {
                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // NO need to show the dialog;
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            //  GPS turned off, Show the user a dialog
                            try {
                                // Show the dialog by calling startResolutionForResult(), and check the result in onActivityResult().
                                // In the onActivityResult method you should have to catch the result and do the rest of the work.
                                ResolvableApiException resolvable = (ResolvableApiException) exception;
                                resolvable.startResolutionForResult(activity, REQUEST_CHECK_SETTINGS);
                            } catch (IntentSender.SendIntentException e) {

                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            break;
                    }
                }

            }
        });
    }

    private void displayLocation(Location location) {

        mLastLocation = location;

        if (mLastLocation != null) {
            latitude = mLastLocation.getLatitude();
            longitude = mLastLocation.getLongitude();
        }
    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(activity, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(activity, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }

        // Used to request periodic updates from the fused location provider. Updates our app periodically with the best available location
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (mGoogleApiClient != null) {
            if (ActivityCompat.checkSelfPermission(activity, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(activity, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            }

            //Now we can make read and write calls using the Location Api's our app is authorized for
            mFusedLocationClient.getLastLocation().addOnSuccessListener(activity, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    // Got last known location. In some rare situations, this can be null.
                    if (location != null) {
                        // returns a Task that we can use to get a Location object with the latitude and longitude coordinates of the user
                        mLastLocation = location;
                    }
                }
            });
            startLocationUpdates();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        removeUpdates();
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        removeUpdates();
        // Displaying the new location on UI
        displayLocation(location);
    }

    private void removeUpdates() {
        //Used when want to stop the location updates when the activity is no longer in focus
        if (servicesAvailable && mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
            // Destroy the current location client
            mGoogleApiClient = null;
        }
    }

    private boolean isLocationEnabled(Context context) {
        int locationMode = 0;

        try {
            locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
            return false;
        }

        return locationMode != Settings.Secure.LOCATION_MODE_OFF;

    }

}
